package ru.kombitsi.tm.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import ru.kombitsi.tm.dto.ProjectDto;
import ru.kombitsi.tm.dto.TaskDto;
import ru.kombitsi.tm.entity.Task;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TaskController {
    final String URL = "http://localhost:8080/tasks";

    @GetMapping("/tasks/showall")
    public List<TaskDto> getAllTasks() {
        final RestTemplate restTemplate = new RestTemplate();
        final TaskDto[] tasks = restTemplate.getForObject(URL+"/showall", TaskDto[].class);
        return Arrays.asList(tasks);
    }

    @GetMapping("/tasks/{id}")
    public TaskDto getOneTaskById(@PathVariable(name = "id") String id) {
        final RestTemplate restTemplate = new RestTemplate();
        final TaskDto taskDto = restTemplate.getForObject(URL+"/"+id, TaskDto.class);
        return taskDto;
    }

    @GetMapping("/tasks")
    public void createTask(@RequestParam String projectId, @RequestParam String name, @RequestParam String description) {
        final RestTemplate restTemplate = new RestTemplate();
        TaskDto taskDto = new TaskDto();
        taskDto.setProjectId(projectId);
        taskDto.setName(name);
        taskDto.setDescription(description);
        restTemplate.postForObject(URL, taskDto, TaskDto.class);
    }

    @GetMapping("/tasks/update")
    public void updateTask(@RequestParam String id, @RequestParam String name, @RequestParam String description ) {
        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put("id", id);
        final RestTemplate restTemplate = new RestTemplate();
        TaskDto taskDto = new TaskDto();
        taskDto.setName(name);
        taskDto.setDescription(description);
        restTemplate.put(URL+"/update/{id}", taskDto, params);
    }

    @GetMapping("tasks/delete")
    public void deleteTask(@RequestParam String id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL+"/delete/{id}", id);
    }
}
